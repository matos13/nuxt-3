import { type } from 'os'

export enum Color {
  Red = 'red',
  Blue = 'blue',
  Green = 'green',
  Yellow = 'yellow'
}
export type ArrayCoba = Array<{
  id: number
  nama: string
  umur: number
  status: boolean
}>
